<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Group;
use App\Entity\User;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $group = new Group();
        $group->setLabel('MMI');
        $manager->persist($group);
        for($i = 0; $i <= 20; $i++) {
            $user = new User();
            $user->setEmail('bastien'.$i.'@gmail.com');
            $user->setPassword(password_hash("edck,s",PASSWORD_DEFAULT));
            $user->setFirstName('yoman'.$i);
            $user->setLastName('laporte'.$i);
            $user->setGroupUser($group);

            // Génère un token de 60 caractères pour chaque utilisateur
            $manager->persist($user);
        }

        $manager->flush();
    }
}
