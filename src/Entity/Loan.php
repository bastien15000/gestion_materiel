<?php

namespace App\Entity;

use App\Repository\LoanRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LoanRepository::class)]
class Loan
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'loans')]
    #[ORM\JoinColumn(nullable: false)]
    private $user_loan;

    #[ORM\ManyToOne(targetEntity: Ressource::class, inversedBy: 'loans')]
    #[ORM\JoinColumn(nullable: false)]
    private $ressource_loan;

    #[ORM\Column(type: 'datetime_immutable')]
    private $created_at;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $finished_at;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $returned_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserLoan(): ?User
    {
        return $this->user_loan;
    }

    public function setUserLoan(?User $user_loan): self
    {
        $this->user_loan = $user_loan;

        return $this;
    }

    public function getRessourceLoan(): ?Ressource
    {
        return $this->ressource_loan;
    }

    public function setRessourceLoan(?Ressource $ressource_loan): self
    {
        $this->ressource_loan = $ressource_loan;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getFinishedAt(): ?\DateTimeImmutable
    {
        return $this->finished_at;
    }

    public function setFinishedAt(?\DateTimeImmutable $finished_at): self
    {
        $this->finished_at = $finished_at;

        return $this;
    }

    public function getReturnedAt(): ?\DateTimeImmutable
    {
        return $this->returned_at;
    }

    public function setReturnedAt(?\DateTimeImmutable $returned_at): self
    {
        $this->returned_at = $returned_at;

        return $this;
    }
}
