<?php

namespace App\Controller;

use App\Repository\GroupRepository;
use App\Entity\Group;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiGroupController extends AbstractController
{
    #[Route('/api/group', name: 'api_group_index', methods: 'GET')]
    public function index(GroupRepository $groupRepository): Response
    {
        return $this->json($groupRepository->findAll(), 200, [], ['groups' => 'group:read']);
    }

    #[Route('/api/group', name: 'api_group_store', methods: 'POST')]
    public function store(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        
        try {
            $jsonRecu = $request->getContent();
    
            $group = $serializer->deserialize($jsonRecu, Group::class, 'json');
    
            $errors = $validator->validate($group);

            if(count($errors) > 0) {
                return $this->json($errors, 400);
            }
 
            $em->persist($group);
            $em->flush();
    
            return $this->json($group, 201, [], ['groups' => 'group:read']);

        } catch(NotEncodableValueException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage(),
            ]);
        }
    }
}
