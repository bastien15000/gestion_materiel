<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiUserController extends AbstractController
{
    #[Route('/api/user', name: 'api_user_index', methods: 'GET')]
    public function index(UserRepository $userRepository): Response
    {
        return $this->json($userRepository->findAll(), 200, [], ['groups' => 'users:read']);
    }

    #[Route('/api/user', name: 'api_user_store', methods: 'POST')]
    public function store(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        try {
            $jsonRecu = $request->getContent();
    
            $user = $serializer->deserialize($jsonRecu, User::class, 'json');
    
            $errors = $validator->validate($user);

            if(count($errors) > 0) {
                return $this->json($errors, 400);
            }
 
            $em->persist($user);
            $em->flush();
    
            return $this->json($user, 201, [], ['groups' => 'users:read']);

        } catch(NotEncodableValueException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage(),
            ]);
        }
    }

    #[Route('/api/user/{id}', name: 'api_user_by_id', methods: 'GET')]
    public function getUserById(UserRepository $userRepository, Request $request): Response
    {
        return $this->json($userRepository->find($request->attributes->get('id')), 200, [], ['groups' => 'users:read']);
    }

    #[Route('/api/user/{id}', name: 'api_user_modify', methods: 'PUT')]
    public function update(User $user, serializerInterface $serializer, Request $request, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        $jsonRecu = json_decode($request->getContent());

        $errors = $validator->validate($user);

        if(count($errors) > 0) {
            return $this->json($errors, 400);
        } else {
            if(isset($jsonRecu->email)) {
                $user->setEmail($jsonRecu->email);
            }
            if(isset($jsonRecu->password)) {
                $user->setPassword(password_hash($jsonRecu->password, PASSWORD_DEFAULT));
            }
            if(isset($jsonRecu->firstname)) {
                $user->setFirstname($jsonRecu->firstname);
            }
            if(isset($jsonRecu->lastname)) {
                $user->setLastname($jsonRecu->lastname);
            }
        }

        $em->persist($user);
        $em->flush();

        return $this->json($user, 201, [], []);
    }

    #[Route('/api/user/{id}', name: 'api_user_delete', methods: 'DELETE')]
    public function delete(User $user, EntityManagerInterface $em): Response
    {
        $em->remove($user);
        $em->flush();

        return $this->json([], 204, [], ['groups' => 'users:read']);
    }
}
